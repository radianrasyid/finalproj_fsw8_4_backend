'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'Users',
      'sellerId',
      {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: "Sellers",
          },
          key: "id"
        }
      }
    )
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.deleteColumn(
      'Users',
      'sellerId'
    )
  }
};
