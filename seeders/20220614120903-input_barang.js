'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Barangs', [
      {
        nama_barang: "Asus Vivobook",
        price: 2000000,
        discount: 0,
        berat: 1.2,
        description: "This laptop is a good one, you should spend your money for this one for sure",
        quantity: 10,
        postedBy: 55,
        categoryId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama_barang: "Lenovo Thinkpad",
        price: 3500000,
        discount: 0,
        berat: 1.5,
        description: "This laptop is a good one, you should spend your money for this one for sure",
        quantity: 10,
        postedBy: 55,
        categoryId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama_barang: "Acer Swift",
        price: 4000000,
        discount: 0.2,
        berat: 1.2,
        description: "This laptop is a good one, you should spend your money for this one for sure",
        quantity: 5,
        postedBy: 55,
        categoryId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
  ])
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Barangs', null, {})
  }
};
