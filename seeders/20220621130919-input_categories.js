'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Categories',[
      {
        nama: "ELEKTRONIK",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "PAKAIAN",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "OLAHRAGA",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "MUSIK",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "MAINAN",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ])
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Categories', null, {})
  }
};
