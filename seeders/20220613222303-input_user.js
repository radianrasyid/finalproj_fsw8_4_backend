'use strict';

const {encryptPassword} = require("../controllers/authController")

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Users', [
      {
        firstName: "Radian",
        lastName: "Rasyid",
        fullname: "Radian Rasyid",
        username: "radianrasyid",
        address: "Rumah",
        userRoleId: 2,
        encryptedPassword: await encryptPassword('123456'),
        email: "radianrasyid@gmail.com",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        firstName: "Lalu",
        lastName: "Oki",
        fullname: "Lalu Oki",
        username: "laluoki",
        address: "Rumah",
        userRoleId: 2,
        encryptedPassword: await encryptPassword('123456'),
        email: "laluoki@gmail.com",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        firstName: "Daffa",
        lastName: "Shidqi",
        fullname: "Daffa Shidqi",
        username: "daffashidqi",
        address: "Rumah",
        userRoleId: 2,
        encryptedPassword: await encryptPassword('123456'),
        email: "daffashidqi@gmail.com",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        firstName: "Djaya",
        lastName: "Dhinata",
        fullname: "Djaya Dhinata",
        username: "djayadhinata",
        address: "Rumah",
        userRoleId: 2,
        encryptedPassword: await encryptPassword('123456'),
        email: "djayadhinata@gmail.com",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        firstName: "Tazkia",
        lastName: "Athariza",
        fullname: "Tazkia Athariza",
        username: "tazkiaathariza",
        address: "Rumah",
        userRoleId: 2,
        encryptedPassword: await encryptPassword('123456'),
        email: "tazkiaathariza@gmail.com",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        firstName: "Super",
        lastName: "Admin",
        fullname: "Super Admin",
        username: "superadmin",
        address: "Rumah",
        userRoleId: 1,
        encryptedPassword: await encryptPassword('123456'),
        email: "superadmin@gmail.com",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ])
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Users', null, {})
  }
};
