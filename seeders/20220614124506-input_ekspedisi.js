'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert("Ekspedisis", [
      {
        name: "JNE",
        pricePerKilo: 49000,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "JNT",
        pricePerKilo: 49000,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "SICEPAT",
        pricePerKilo: 49000,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
