'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('notificationCategories', [
      {
        type: "Penawaran Produk",
        text: "Ordering your stuff",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        type: "Berhasil Diterbitkan",
        text: "Has been created",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
