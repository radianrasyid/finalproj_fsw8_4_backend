'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Notifikasis', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      barangId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: "Barangs",
          },
          key: "id"
        }
      },
      harga: {
        type: Sequelize.INTEGER
      },
      tawar: {
        type: Sequelize.INTEGER
      },
      status: {
        type: Sequelize.STRING,
      },
      seen_by_user: {
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: "Users",
          },
          key: "id"
        }
      },
      senderId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: "Users",
          },
          key: "id"
        }
      },
      categoryId:{
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: "notificationCategories"
          },
          key: "id"
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Notifikasis');
  }
};
