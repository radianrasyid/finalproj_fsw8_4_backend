'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'Barangs',
      'isAvailable',
      {
        type: Sequelize.INTEGER
      }
    )
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.removeColumn(
      'Barangs',
      'isAvailable'
    )
  }
};
