'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'Notifikasis',
      'deletedAt',
      {
        allowNull: true,
        type: Sequelize.DATE
      }
    )
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.removeColumn(
      'Notifikasis',
      'deletedAt'
    )
  }
};
