const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const app = express();
const userController = require("./controllers/userController");
const authController = require("./controllers/authController");
const barangController = require("./controllers/barangController");
const photoController = require("./controllers/photoController");
const notifController = require("./controllers/notificationController");
const emailController = require("./controllers/emailController");
const negoController = require("./controllers/negoController");
const minatController = require("./controllers/minatController");
const swaggerUI = require("swagger-ui-express");
const swaggerDocument = require("./docs/swagger.json");
const cors = require("cors");
const { get } = require("http");
const cookieParser = require("cookie-parser");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// app.use(express.json());
app.use(
  cors({
    origin: "*",
  })
);
app.use(cookieParser());

app.get("/", (req, res) => {
  res.status(200).json({
    message: "BERHASIL",
  });
});

// USER
app.get("/api/user", authController.authorizeAdmin, userController.listUser);
app.post("/api/users/register", userController.registerAdmin);
app.post("/api/users/registeruser", userController.registerUser);
app.post("/api/users/login", userController.login);
app.delete(
  "/api/users/delete/:id",
  authController.authorizeAdmin,
  userController.delete
);
app.put(
  "/api/users/update",
  authController.authorizeUser,
  userController.updateUser
);
app.get("/api/users/data", userController.getUser);

// SELLER
app.post(
  "/api/sellers/registerseller",
  authController.authorizeUser,
  userController.registerSeller
);
app.put(
  "/api/sellers/updateseller",
  authController.authorizeUser,
  userController.updateSeller
);
app.get(
  "/api/sellers/barangseller",
  authController.authorizeUser,
  barangController.listBarangSeller
);
app.delete(
  "/api/sellers/deleteseller",
  authController.authorizeUser,
  userController.deleteSeller
);
app.get(
  "/api/sellers/sellerdata",
  authController.authorizeUser,
  userController.getSeller
);
app.get(
  "/api/sellers/minat",
  authController.authorizeUser,
  minatController.listMinat
)

// BARANG
app.post(
  "/api/items/create",
  authController.authorizeUser,
  barangController.barangPost
);
app.get("/api/items/findall", barangController.barangFindAll);
app.put(
  "/api/items/update/:id",
  authController.authorizeUser,
  barangController.barangUpdate
);
app.delete(
  "/api/items/delete/:id",
  authController.authorizeUser,
  barangController.deleteBarang
);
app.get(
  "/api/items/find/:id",
  authController.authorizeUser,
  barangController.barangFindOne
);
app.get("/api/items/findone/:id", barangController.barangFindOneId);
app.get("/api/items/findonesell/:id", authController.authorizeUser, barangController.barangFindOneIdSeller);
app.delete("/api/items/destroyall", barangController.destroyAll);
app.put("/api/items/terbitkan/:id", authController.authorizeUser, barangController.barangTerbit)
app.get("/api/items/findall/sold", authController.authorizeUser, barangController.findTerjual)

// NOTIFICATION
app.post(
  "/api/notifications/terbit",
  authController.authorizeUser,
  notifController.createNotifCreate
);
app.post(
  "/api/notifications/tawar/:id",
  authController.authorizeUser,
  notifController.createNotifTawar
);
app.delete(
  "/api/notifications/delete/:id",
  authController.authorizeUser,
  notifController.deleteNotifById
);
app.delete(
  "/api/notifications/delete",
  authController.authorizeUser,
  notifController.deleteAllNotif
);
app.put(
  "/api/notifications/update/:id",
  authController.authorizeUser,
  notifController.updateNotifById
);
app.put(
  "/api/notifications/update",
  authController.authorizeUser,
  notifController.updateNotif
);
app.get(
  "/api/notifications/read",
  authController.authorizeUser,
  notifController.readNotif
);

app.post("/uploadphotoitem", photoController.item, photoController.postPhoto);

// NEGOSIASI
app.get(
  "/api/negos/findall",
  authController.authorizeUser,
  negoController.findNego
);
app.get(
  "/api/negos/find/:id",
  authController.authorizeUser,
  negoController.findNegoById
);
app.get("/api/negos/findfor/:id", authController.authorizeUser, negoController.findNegoforUser)

app.post(
  "/api/negos/create/:id",
  authController.authorizeUser,
  negoController.createNego
);

app.post(
  "/api/negos/directbuy/:id",
  authController.authorizeUser,
  negoController.directBuy
)

app.put(
  "/api/negos/directbuyupdate/:id",
  authController.authorizeUser,
  negoController.directBuyUpdate
)

app.put(
  "/api/negos/update/:id",
  authController.authorizeUser,
  negoController.updateNego
);

app.put(
  "/api/negos/updatereject/:id",
  authController.authorizeUser,
  negoController.updateNegoBarang
)

app.put(
  "/api/negos/updateterjual/:id",
  authController.authorizeUser,
  negoController.negoTerjual
)

app.put(
  "/api/negos/updateterjualtolak/:id",
  authController.authorizeUser,
  negoController.negoTerjualTolak
)

app.get("/testdev", authController.checkdev);
app.get(
  "/testdev/barang/:id",
  authController.authorizeUser,
  barangController.testdevbarang
);

app.post("/sendemail", emailController.reset);
app.post("/updatepassword/:token", emailController.updatePassword);

app.use("/documentation", swaggerUI.serve, swaggerUI.setup(swaggerDocument));

app.listen(process.env.PORT || 8080, function () {
  console.log(
    "Express server listening on port %d in %s mode",
    this.address().port,
    app.settings.env
  );
  console.log(`http://localhost:${this.address().port}`);
});
