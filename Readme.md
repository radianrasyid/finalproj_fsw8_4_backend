# Second Hand Back End KELOMPOK 4 - FSW 8 Batch 2

## Members For this particular project 👨‍👧‍👧 :

```
- Lalu Ocky Saktiya Luhung
- Muhammad Daffa Shidqi
- Muhammad Radian Rasyid

```

## Whole Squad :

```

- Tazkia Athariza Dhivara
- Muhammad Radian Rasyid
- Muhammad Daffa Shidqi
- Lalu Ocky Saktiya Luhung
- Djaya Dhinata

```

# Description

```
## To Run This Project :
 * Clone this project to your local environment
 * Run __npm install__ to install all lists of dependencies
 * Run __npm start__ and open the link that has been provided in your terminal

```

## Tech

| Package            | Version      | Source                                                  |
| ------------------ | ------------ | ------------------------------------------------------- |
| bcrypt             | ^5.0.1       | https://www.npmjs.com/package/bcrypt                    |
| express            | ^4.18.1      | https://expressjs.com/                                  |
| cors               | ^2.8.5       | https://expressjs.com/en/resources/middleware/cors.html |
| jsonwebtoken       | ^8.5.1       | https://jwt.io/                                         |
| pg                 | ^8.7.3       | https://yarnpkg.com/package/pg                          |
| pg-hstore          | ^2.3.4       | https://yarnpkg.com/package/pg-hstore                   |
| sequelize          | ^6.19.0      | https://sequelize.org/                                  |
| sequelize-cli      | ^6.4.1       | https://yarnpkg.com/package/sequelize-cli               |
| swagger-ui-express | ^4.4.0       | https://www.npmjs.com/package/swagger-ui-express        |
| multer             | ^1.4.5-lts.1 | https://www.npmjs.com/package/multer                    |
| cookie-parser      | ^1.4.6       | https://www.npmjs.com/package/cookie-parser             |
| body-parser        | ^1.20.0      | https://www.npmjs.com/package/body-parser               |
| cloudinary         | ^1.30.0      | https://www.npmjs.com/package/cloudinary                |
