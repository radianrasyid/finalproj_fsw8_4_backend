const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { User } = require("../models");
const nodemailer = require("nodemailer");
const admin = require("../controllers/firebaseController");

module.exports = {
  encryptPassword(password) {
    return new Promise((resolve, reject) => {
      bcrypt.hash(password, 10, (err, encryptedPassword) => {
        if (!!err) {
          reject(err);
          return;
        }

        resolve(encryptedPassword);
      });
    });
  },

  comparePassword(encryptedPassword, password) {
    return new Promise((reject, resolve) => {
      bcrypt.compareSync(
        password,
        encryptedPassword,
        (err, isPasswordCorrect) => {
          if (!!err) {
            reject(err);
            return;
          }

          resolve(isPasswordCorrect);
        }
      );
    });
  },

  async createToken(payload) {
    return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "Rahasia");
  },

  async check(requestBody, res) {
    try {
      const email = requestBody.body.email.toLowerCase();
      const pass = requestBody.body.password;

      const user = await User.findOne({
        where: { email },
        include: ["userRoles"],
      });

      if (!user) {
        res.status(404).json({ message: "Can't find that account" });
        return;
      }

      const isPasswordCorrect = await comparePassword(
        user[0].dataValues.encryptedPassword,
        pass
      );

      if (!isPasswordCorrect) {
        res.status(401).json({ message: "Can't find that account" });
        return;
      }

      const token = this.createToken({
        id: user[0].dataValues.id,
        email: user[0].dataValues.email,
        firstName: user[0].dataValues.firstName,
        lastName: user[0].dataValues.lastName,
        fullname: user[0].dataValues.fullname,
        username: user[0].dataValues.username,
        address: user[0].dataValues.address,
      });

      const newToken = {
        nama: user[0].name,
        ID: user[0].id,
        Identification: user[0].userRoles.name,
        token,
      };

      return newToken;
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "FAIL",
      });
    }
  },

  async authorizeUser(req, res, next) {
    try {
      try {
        const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );
      const id = tokenPayload.id;
      const requestUser = await User.findByPk(tokenPayload.id, {
        include: ["userRoles", "sellerid"],
      });
      const user = await User.findOne({
        where: { id },
        include: ["userRoles", "sellerid"],
      });
      if (requestUser.userRoleId) {
        req.result = {
          user: {
            name: user.dataValues.fullname,
            position: user.dataValues.userRoles.name,
            roleId: user.dataValues.userRoles.id,
          },
        };

        req.tokenPayload = tokenPayload;

        next();
      }
      } catch (error) {
          try {
            const decodeValue = await admin.auth().verifyIdToken(token);
            if (decodeValue) {
              req.tokenPayload = decodeValue;
              return next();
            }
  
            res.json({
              message: "Unauthorized Access",
            });
          } catch (error) {
            res.json({
              message: "Internal Server Error",
              error,
            });
          }
          return;
      }
    } catch (err) {
      console.log(err);
      res.status(403).json({
        status: "FAILED",
        message: "Unauthorized Access",
      });
    }
  },

  async authorizeAdmin(req, res, next) {
    try {
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );
      const id = tokenPayload.id;
      const requestUser = await User.findByPk(tokenPayload.id, {
        include: ["userRoles"],
      });
      const user = await User.findOne({
        where: { id },
        include: ["userRoles"],
      });
      if (requestUser.userRoleId == 2 || requestUser.userRoleId == 7) {
        req.result = {
          user: {
            name: user.dataValues.fullname,
            position: user.dataValues.userRoles.name,
          },
        };

        req.tokenPayload = tokenPayload;

        next();
      }
    } catch (err) {
      console.log(err);
      res.status(403).json({
        status: "FAILED",
        message: "Unauthorized Access",
      });
    }
  },

  async AuthorizeSuperAdmin(req, res, next) {
    try {
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );
      const id = tokenPayload.id;
      const requestUser = await User.findByPk(tokenPayload.id, {
        include: ["userRoles"],
      });
      const user = await User.findOne({
        where: { id },
        include: ["userRoles"],
      });
      if (requestUser.userRoleId == 1) {
        req.result = {
          user: {
            name: user.dataValues.fullname,
            position: user.dataValues.userRoles.name,
          },
        };

        req.tokenPayload = tokenPayload;

        next();
      }
    } catch (err) {
      console.log(err);
      res.status(403).json({
        status: "FAILED",
        message: "Unauthorized Access",
      });
    }
  },

  async AuthorizeFirebase(req, res, next) {
    const token = await req.headers.authorization.split("Bearer ")[1];

    try {
      const decodeValue = await admin.auth().verifyIdToken(token);
      if (decodeValue) {
        req.tokenPayload = decodeValue;
        return next();
      }

      res.json({
        message: "Unauthorized Access",
      });
    } catch (error) {
      res.json({
        message: "Internal Server Error",
        error,
      });
    }
  },

  async AuthorizeCompletion(req, res, next) {
    try {
      const token = req.tokenPayload;
      const id = token.id;
      const user = await User.findAll({ where: { id: id } });
      const userrect = user[0].dataValues;

      if (
        userrect.id != null &&
        userrect.firstName != null &&
        userrect.lastName != null &&
        userrect.fullname != null &&
        userrect.username != null &&
        userrect.email != null &&
        userrect.address != null &&
        userrect.phoneNumber != null &&
        userrect.sellerId != null
      ) {
        next();
      }

      res.redirect("/update");
    } catch (error) {
      console.log(error);
      res.status(403).redirect("/register");
    }
  },

  async checkdev(req, res) {
    try {
      const user = await User.findAll();
      res.json({
        user: user,
      });
      req.setReq;
    } catch (error) {
      res.status(404).json({
        message: error,
      });
    }
  },

  sendEmail(email, token) {
    var mail = nodemailer.createTransport({
      service: "Gmail",
      auth: {
        user: "kelompok4fswbinar@gmail.com",
        pass: "jnhgewdazrxhirjw",
      },
    });

    var mailOptions = {
      from: "kelompok4fswbinar@gmail.com",
      to: email,
      subject: "Reset Password Link - kelompok4anjaygurinjay.com",
      html:
        '<p>You requested for reset password, kindly use this <a href="https://finalproj-fsw8-4-frontend.vercel.app/gantipassword/' +
        token +
        '">link</a> to reset your password</p>',
    };

    mail.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(1);
      } else {
        console.log(0);
      }
    });
  },

  async sendEmailPassword(email, value) {
    var mail = nodemailer.createTransport({
      service: "Gmail",
      auth: {
        user: "kelompok4fswbinar@gmail.com",
        pass: "jnhgewdazrxhirjw",
      },
    });

    var mailOptions = {
      from: "kelompok4fswbinar@gmail.com",
      to: email,
      subject: "Password Confirmation - kelompok4anjaygurinjay.com",
      html:
        `<p>Berikut adalah password kamu secara default : <b>${value}</b></p>
        <br>
        <p>Jangan dishare ke orang lain ya..</p>`,
    };

    mail.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(1);
      } else {
        console.log(0);
      }
    });
  },
};
