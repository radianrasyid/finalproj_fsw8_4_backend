const nodemailer = require("nodemailer");
const { User, Seller, Stat, Barang } = require("../models");
const jwt = require("jsonwebtoken");
const randtoken = require('rand-token');
const bcrypt = require("bcryptjs");
const authController = require("./authController");

module.exports = {

  async reset(req, res){
    let email = req.body.email;


    const user = await User.findAll({where: {email}})

    if(!user){
        res.status(404).json({
            message: 'account not found'
        })
    }

    const token = jwt.sign({email: email},
        process.env.JWT_SIGNATURE_KEY || "Rahasia", {
            expiresIn: 120000
        });

    const sent = await authController.sendEmail(email, token);

    await User.update({
        resetLink: token
    }, {where: {email}})

    res.json({
        message: "email sent"
    })
  },

  async updatePassword(req, res, next){
    let token = req.params.token;
    console.log("TOKEN", token);
    let password = req.body.password;

    const isVerified = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "Rahasia");

    if(!isVerified){
        res.json({
            message: "Token expired"
        })
    }

    const user = await User.findAll({where: {
        resetLink:token
    }})

    if(!user){
        res.json({
            message: "Failed on resetting your password"
        })
    }

    await User.update({
        encryptedPassword: await authController.encryptPassword(password),
        resetLink: ""
    }, {where: {resetLink: token}})

    res.json({
        message: "your password has been updated"
    })
  }
};
