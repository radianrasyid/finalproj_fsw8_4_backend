const {
  Barang,
  User,
  Seller,
  Negosiasi,
  Category,
  Sequelize,
  Photo,
} = require("../models");
const upload = require("../upload");
const uploadOnMemory = require("../uploadOnMemory");
const cloudinary = require("../cloudinary");
const authController = require("./authController");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const {
  Op,
  where
} = require("sequelize");

module.exports = {
  async barangPost(req, res) {
    try {
      const token = req.tokenPayload;
      const sllrId = token.id;
      console.log("SELLERID", token.sellerId);

      const isSellerIdCorrect = () => {
        return;
      };

      if (!isSellerIdCorrect) {
        console.log("Hasil", isUsernameCorrect);
        res.status(403).json({
          message: "Create account as seller before selling products",
        });

        return;
      }

      uploadOnMemory.array("picture")(req, res, async function () {
        let pics = [];
        if (!req.files[0].buffer) {
          return res.json({
            message: "Something went wrong"
          })
        }
        for (let i = 0; i < req.files.length; i++) {
          const fileBase64 = req.files[i].buffer.toString("base64");
          const file = `data:${req.files[i].mimetype};base64,${fileBase64}`;
          const url = `/uploads/$`;

          await cloudinary.uploader.upload(file, async function (err, result) {
            if (err) {
              console.log(err);
            }
            pics.push(result.url)
          });
        }

        await Barang.create({
          nama_barang: req.body.nama_barang ? req.body.nama_barang : "",
          price: req.body.price ? Number(req.body.price) : 10000,
          description: req.body.description ? req.body.description : "",
          categoryId: req.body.category ? Number(req.body.category) : 1,
          image: pics ? pics : "",
          postedBy: Number(token.id),
          isAvailable: 0,
        });

        res.status(201).json({
          message: "Berhasil create item",
        })
      });
    } catch (error) {
      res.json({
        message: "ERROR ON UPLOADING FILE",
        error: error
      })
    }
  },

  async barangFindAll(req, res) {
    try {
      const item = await Barang.findAll({
        where: {
          isAvailable: 1,
        },
        include: ["postedby", "categoryid"]
      });
      res.status(200).json(item)
    } catch (error) {
      res.send(error);
    }
  },

  async barangFindOne(req, res) {
    try {
      let id = req.params.id;
      let item = await Barang.findAll({
        where: {
          categoryId: id
        }
      });
      let count = await Barang.count({
        where: {
          categoryId: id
        }
      });
      res.status(200).json({
        message: "is this what you looking for?",
        jumlah: count,
        list: item,
      });
    } catch (error) {
      res.send(error);
    }
  },

  async barangFindOneId(req, res) {
    let id = req.params.id;
    let item = await Barang.findAll({
      where: {
        [Op.and]: [
          {
            id: id,
          },
          {
            isAvailable: 1,
          }
        ]
      },
      include: ["postedby", "categoryid"]
    });

    res.status(200).json({
      message: "This is your item",
      barang: item,
    });
  },

  async barangUpdate(req, res) {
    try {
      const token = req.tokenPayload;
      const sllrId = token.id;
      const barangFrom = await Barang.findAll({
        where: {
          [Op.and]: [{
            id: req.params.id
          }, {
            postedBy: sllrId
          }],
        },
      });

        uploadOnMemory.array("picture")(req, res, async function () {
          let pics = [];
          // let picsChange=await barangFrom[0].dataValues.image;
          console.log("INI DATA BARANGFROM", barangFrom[0].dataValues.image)
          if(!req.files){
            await Barang.update({
            nama_barang: req.body.nama_barang ?
              req.body.nama_barang : barangFrom[0].dataValues.nama_barang,
            image: picsChange,
            price: req.body.price ?
              Number(req.body.price) : barangFrom[0].dataValues.price,
            discount: req.body.discount ?
              Number(req.body.discount) : barangFrom[0].dataValues.discount,
            berat: req.body.berat ?
              Number(req.body.berat) : barangFrom[0].dataValues.berat,
            description: req.body.description ?
              req.body.description : barangFrom[0].dataValues.description,
            quantity: req.body.quantity ?
              Number(req.body.quantity) : barangFrom[0].dataValues.quantity,
            categoryId: req.body.category ?
              Number(req.body.category) : barangFrom[0].dataValues.categoryId,
          }, {
            where: {
              [Op.and]: [{
                id: req.params.id
              }, {
                postedBy: sllrId
              }],
            },
          });

          res.status(204).json({
            message: "barang successfully updated"
          })

          return;
        }

          for (let i = 0; i < req.files.length; i++) {
            const fileBase64 = req.files[i].buffer.toString("base64");
            const file = `data:${req.files[i].mimetype};base64,${fileBase64}`;
            const url = `/uploads/$`;

            await cloudinary.uploader.upload(file, async function (err, result) {

              picsChange.push(result.url)
            });
          }

          console.log("INI PICTURE ARRAY", picsChange)

          const barang = await Barang.update({
            nama_barang: req.body.nama_barang ?
              req.body.nama_barang : barangFrom[0].dataValues.nama_barang,
            image: picsChange ? picsChange : barangFrom[0].dataValues.image,
            price: req.body.price ?
              Number(req.body.price) : barangFrom[0].dataValues.price,
            discount: req.body.discount ?
              Number(req.body.discount) : barangFrom[0].dataValues.discount,
            berat: req.body.berat ?
              Number(req.body.berat) : barangFrom[0].dataValues.berat,
            description: req.body.description ?
              req.body.description : barangFrom[0].dataValues.description,
            quantity: req.body.quantity ?
              Number(req.body.quantity) : barangFrom[0].dataValues.quantity,
            categoryId: req.body.category ?
              Number(req.body.category) : barangFrom[0].dataValues.categoryId,
          }, {
            where: {
              [Op.and]: [{
                id: req.params.id
              }, {
                postedBy: sllrId
              }],
            },
          });

          res.status(204).json({
            message: "barang successfully updated with new images"
          })
        });
    } catch (error) {
      res.status(400).json({
        status: "FAIL",
        message: error.message,
      });
    }
  },

  async barangFindOneIdSeller(req, res) {
    const token = req.tokenPayload
    let id = req.params.id;
    let item = await Barang.findAll({
      where: {
        [Op.and]: [
          {
            id: id,
          },
          {
            postedBy: token.id
          }
        ]
      },
      include: ["postedby", "categoryid"]
    });

    res.status(200).json({
      message: "This is your item",
      barang: item,
    });
  },

  async barangTerbit(req, res) {
    try {
      const idBarang = req.params.id;
      const token = req.tokenPayload;

      const barang = await Barang.findAll({
        where: {
          [Op.and]: [{
              id: idBarang,
            },
            {
              postedBy: token.id
            }
          ]
        }
      })

      if(!barang){
        return res.json({
          message: "You don't have access for this item"
        })
      }

      await Barang.update({
        isAvailable: 1
      }, {
        where: {
          id: barang[0].dataValues.id
        }
      })

      res.json({
        message: "Barang has been updated"
      })
    } catch (error) {
      console.log(error);
    }
  },

  async deleteBarang(req, res) {
    try {
      const token = req.tokenPayload;
      const sllrId = token.sellerId;
      const idbarang = req.params.id;
      const id = token.id;

      const user = await User.findAll({
        where: {
          id: id
        },
        include: ["userRoles", "sellerid"],
      });
      const result = await Barang.destroy({
        where: {
          [Op.and]: [{
            id: idbarang
          }, {
            postedBy: id
          }],
        },
      });
      if (!result) {
        res.status(404).json({
          deletedby: req.result,
          message: "Barang not found",
        });
      }
      res.json({
        message: "Barang successfully deleted",
      });
    } catch (error) {
      res.status(400).json({
        status: "FAIL",
        message: error.message,
      });
    }
  },

  async listBarangSeller(req, res) {
    try {
      const token = req.tokenPayload;
      const userId = token.id;
      const user = await User.findAll({
        where: {
          id: userId
        }
      });

      const sellerId = user[0].dataValues.sellerId;
      console.log("SELLER", sellerId);
      const item = await Barang.findAll({
        where: {
          postedBy: userId
        },
        include: ["categoryid", "postedby"]
      });
      const jumlahBarang = await Barang.count({
        where: {
          postedBy: userId
        }
      });

      res.status(200).json({
        jumlah_barang: jumlahBarang,
        barang: item,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "FAIL",
        message: "Something is wrong",
      });
    }
  },

  async testdevbarang(req, res) {
    const id = req.params.id;
    const token = req.tokenPayload;
    const userId = token.id;
    const baru = await Barang.findAll({
      where: {
        [Op.and]: [{
          id: id
        }, {
          postedBy: userId
        }],
      },
    });
    console.log(baru);
  },

  async destroyAll(req, res) {
    Barang.destroy({
      where: {},
      truncate: true
    })

    res.json({
      message: "ALL BARANG SUCCESSFULLY DESTROYED"
    })
  },

  async findTerjual(req, res){
    try{
    const token = req.tokenPayload;

    const nego = await Negosiasi.findAll({
      where: {
        [Op.and]: [
          {isTerjual: "TERJUAL"},
          {receiverId: token.id}
        ]
      },
      include: ["itemid", "receiverid", "sendid"],
      order: [["updatedAt", "DESC"]]
    })
    
    res.status(200).json(
    {message: "this is your sold items",
    items: nego
    }
    )
    }catch(error){
    console.log(error)
    res.json({
    message: error
    })
    }
    }
    
};