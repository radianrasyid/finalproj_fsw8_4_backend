const auth = require("./authController");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { User } = require("../models");

describe("#authController", () => {
  describe("#encryptPassword", () => {
    it("Should return an encrypted Password", async () => {
      const mPass = "123456";
      const result = await auth.encryptPassword(mPass);

      expect(result).resolves;
    });
  });

  describe("#comparePassword", () => {
    it("should return boolean from comparing password", async () => {
      const mPass = "123456";
      const mResult = await auth.encryptPassword(mPass);

      const result = bcrypt.compareSync(mPass, mResult);

      expect(result).toBeTruthy();
    }, 60000);
  });

  describe("#createToken", () => {
    it("should return a token created", async () => {
      const payload = {
        nama: "radianrasyid",
        kelas: "fsw8",
        kelompok: "4",
        role: "backend",
      };

      const result = jwt.sign(
        payload,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );

      const app = await auth.createToken(payload);

      expect(app).toEqual(result);
    });
  });

  describe("#check", () => {
    it("should return a token, id, and current user", async () => {
      const email = "kelompok4@gmail.com";
      const pass = "123456";

      const user = {
        firstName: "radian",
        lastName: "rasyid",
        fullname: "radian rasyid",
        username: "radianrasyid",
        address: "jauh la pokoknya",
        phoneNumber: "088888888888",
        userRoleId: 1,
        encryptedPassword: await auth.encryptPassword(pass),
        sellerId: 1,
        email: email,
      };

      const mockRequest = {};
      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const mockUserModel = {};
      const mockUser = new User(user);
      mockUserModel.findOne = jest.fn().mockReturnValue(mockUser);
      const app = new auth({ User: mockUserModel });

      if (!user) {
        const err = new Error();
        await auth.check(mockRequest, mockResponse);
        expect(mockResponse.status).toHaveBeenCalledWith(404);
        expect(mockResponse.json).toHaveBeenCalledWith({
          message: "Can't find that account",
        });
      }

      const isPasswordCorrect = auth.comparePassword(
        user.encryptedPassword,
        pass
      );

      if (!isPasswordCorrect) {
        expect(mockResponse.status).toHaveBeenCalledWith(401);
        expect(mockResponse.json).toHaveBeenCalledWith({
          message: "Can't find that account (Pass)",
        });
      }

      const token = await auth.createToken({
        id: 1,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        fullname: user.fullname,
        username: user.username,
        address: user.address,
      });

      const newToken = {
        nama: user.fullname,
        ID: 1,
        identification: "admin",
        token,
      };

      expect(newToken);
    });

    it("should return console log error when function is rejected", async () => {
      const email = "kelompok4@gmail.com";
      const pass = "123456";

      const user = {
        firstName: "radian",
        lastName: "rasyid",
        fullname: "radian rasyid",
        username: "radianrasyid",
        address: "jauh la pokoknya",
        phoneNumber: "088888888888",
        userRoleId: 1,
        encryptedPassword: await auth.encryptPassword(pass),
        sellerId: 1,
        email: email,
      };

      const mockRequest = {
        body: {
          email: email,
          pass: pass,
        },
      };
      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const mockUserModel = {};
      mockUserModel.findOne = jest.fn().mockReturnValue(user);

      if (!user) {
        const err = new Error();
        await auth.check(mockRequest, mockResponse);
        expect(mockResponse.status).toHaveBeenCalledWith(404);
        expect(mockResponse.json).toHaveBeenCalledWith({
          message: "Can't find that account",
        });
      }

      const isPasswordCorrect = auth.comparePassword(
        user.encryptedPassword,
        pass
      );

      if (!isPasswordCorrect) {
        expect(mockResponse.status).toHaveBeenCalledWith(401);
        expect(mockResponse.json).toHaveBeenCalledWith({
          message: "Can't find that account (Pass)",
        });
      }

      const token = await auth.createToken({
        id: 1,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        fullname: user.fullname,
        username: user.username,
        address: user.address,
      });

      const newToken = {
        nama: user.fullname,
        ID: 1,
        identification: "admin",
        token,
      };

      const hasiltoken = await auth;

      expect(newToken);
    });
  });
});
