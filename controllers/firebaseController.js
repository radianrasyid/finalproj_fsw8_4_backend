
var admin = require("firebase-admin");

var serviceAccount = require("../radianprojies-65666e42b70d.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://radianprojies-default-rtdb.firebaseio.com"
});

module.exports = admin;
