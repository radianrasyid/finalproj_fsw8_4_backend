const multer = require("multer");
const cloudinary = require('cloudinary').v2;

const { CloudinaryStorage } = require("multer-storage-cloudinary");

cloudinary.config({ 
    cloud_name: 'dbihbfpig', 
    api_key: '593289649247291', 
    api_secret: 'EptX8mPaUlGmeqMK0mxn8M10RJU' 
  });

const storageItem = new CloudinaryStorage({
  cloudinary: cloudinary,
  params: {
    folder: "item",
    // format: async (req, file) => "png", // supports promises as well
    // public_id: (req, file) => "computed-filename-using-request",
  },
});

const uploadPhotoItem = multer({ storage: storageItem });

function postPhoto(req, res) {
  try {
    res.status(200).json({
      path: req.file.path,
    });
  } catch (error) {
    res.json({
      message: error,
    });
  }
}

module.exports = {
  item: uploadPhotoItem.single("file"),
  postPhoto,
};
