module.exports = {
  barangPost: require("./barangController"),
  barangFindAll: require("./barangController"),
  barangUpdate: require("./barangController"),
  deleteBarang: require("./barangController"),
};
