const {
    Barang,
    User,
    Seller,
    Category,
    Sequelize,
    Photo,
    Negosiasi,
    Notifikasi,
} = require("../models");

module.exports = {
    async listMinat(req, res) {
        try {
            const token = req.tokenPayload;

            const nego = await Negosiasi.findAll({
                where: {
                    receiverId: token.id
                },
                include: ["itemid", "sendid", "receiverid"],
                order: [["updatedAt", "DESC"]]
            })

            res.status(200).json({
                message: "ini adalah barang anda yang diminati",
                data: nego
            })
        } catch (error) {
            console.log(error);
            res.json({
                message: error
            })
        }
    }
}