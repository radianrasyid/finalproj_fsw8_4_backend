const {
  Op,
  where
} = require("sequelize");
const {
  User,
  Seller,
  Stat,
  Barang
} = require("../models");
const authController = require("./authController");
const uploadOnMemory = require("../uploadOnMemory");
const cloudinary = require("../cloudinary");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const {
  request,
  response
} = require("express");
const cookieParser = require("cookie-parser");
const admin = require("../controllers/firebaseController");
const randToken = require('rand-token');
const {
  sendEmailPassword
} = require("./authController");

module.exports = {
  async registerAdmin(req, res) {
    try {
      if (req.file) {
        uploadOnMemory.single("picture")(req, res, async function () {
          const fileBase64 = req.file.buffer.toString("base64");
          const file = `data:${req.file.mimetype};base64,${fileBase64}`;
          const url = `/uploads/$`;

          cloudinary.uploader.upload(file, async function (err, result) {
            if (err) {
              console.log(err);
              return res.status(400).json({
                message: "gagal upload file",
              });
            }
            const user = await User.create({
              firstName: req.body.firstName ? req.body.firstName : "",
              lastName: req.body.lastName ? req.body.lastName : "",
              fullname: req.body.fullname ? req.body.fullname : "",
              username: req.body.username ? req.body.username : "",
              image: result.url ? result.url : "",
              phoneNumber: req.body.nomorhp ? req.body.nomorhp : "",
              encryptedPassword: await authController.encryptPassword(
                req.body.password
              ),
              email: req.body.email,
              address: req.body.address ? req.body.address : "",
              city: req.body.city ? req.body.city : "",
              userRoleId: 2,
            });

            res.status(201).json({
              status: "OK",
              data: user,
              message: "User succesfully updated",
            });
          });
        });
      } else if (!req.file) {
        const user = await User.create({
          firstName: req.body.firstName ? req.body.firstName : "",
          lastName: req.body.lastName ? req.body.lastName : "",
          fullname: req.body.fullname ? req.body.fullname : "",
          username: req.body.username ? req.body.username : "",
          image: "",
          phoneNumber: req.body.nomorhp ? req.body.nomorhp : "",
          encryptedPassword: await authController.encryptPassword(
            req.body.password
          ),
          email: req.body.email,
          address: req.body.address ? req.body.address : "",
          city: req.body.city ? req.body.city : "",
          userRoleId: 2,
        });

        res.status(201).json({
          status: "OK",
          data: user,
          message: "User succesfully updated",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: "No data to be inserted",
        error,
      });
    }
  },

  async registerUser(req, res) {
    try {
      let defaultPass = randToken.generate(8)

      if (!req.body.password) {
        const user = await User.create({
          firstName: req.body.firstName ? req.body.firstName : "",
          lastName: req.body.lastName ? req.body.lastName : "",
          fullname: req.body.fullname ? req.body.fullname : "",
          username: req.body.username ? req.body.username : "",
          image: "",
          phoneNumber: req.body.nomorhp ? req.body.nomorhp : "",
          encryptedPassword: await authController.encryptPassword(defaultPass),
          email: req.body.email,
          address: req.body.address ? req.body.address : "",
          city: req.body.city ? req.body.city : "",
          userRoleId: 3,
        });

        await authController.sendEmailPassword(req.body.email, defaultPass)

        return res.json({
          message: "Your account has been created, please check your email",
          pass: defaultPass
        })

      }

      const user = await User.create({
        firstName: req.body.firstName ? req.body.firstName : "",
        lastName: req.body.lastName ? req.body.lastName : "",
        fullname: req.body.fullname ? req.body.fullname : "",
        username: req.body.username ? req.body.username : "",
        image: "",
        phoneNumber: req.body.nomorhp ? req.body.nomorhp : "",
        encryptedPassword: await authController.encryptPassword(req.body.password),
        email: req.body.email,
        address: req.body.address ? req.body.address : "",
        city: req.body.city ? req.body.city : "",
        userRoleId: 3,
      });

      res.json({
        status: "OK",
        data: user,
        message: "User succesfully created",
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: "Error on registering user",
        error,
      });
    }
  },

  async registerSeller(req, res) {
    try {
      const token = req.tokenPayload;
      const usernameCon = token.username;
      console.log("SELLERID", token.sellerId);

      const isUsernameCorrect = () => {
        return (req.body.username = usernameCon);
      };

      if (!isUsernameCorrect) {
        console.log("Hasil", isUsernameCorrect);
        res.status(403).json({
          message: "Create account as user before creating a seller account",
        });

        return;
      }

      Stat.create({
        pendapatan: 0,
        sold_prod: 0,
        new_order: 0,
      });

      const hasil = await Stat.findAll({
        limit: 1,
        order: [
          ["createdAt", "DESC"]
        ],
      });

      console.log("HASIL", hasil[0].dataValues.id);

      await Seller.create({
        nama: req.body.name,
        userId: token.id,
        statsId: hasil[0].dataValues.id,
        Status: "SELLER",
      });

      const hasilseller = await Seller.findAll({
        limit: 1,
        order: [
          ["createdAt", "DESC"]
        ],
      });

      const id = token.id;

      await User.update({
        sellerId: hasilseller[0].dataValues.id,
      }, {
        where: {
          id
        }
      });

      res.status(201).json({
        message: "berhasil",
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: "An error occured",
      });
    }
  },

  async login(req, res) {
    try {
      const email = req.body.email || req.body.username;
      const pass = req.body.password ? req.body.password : null;
      const username = req.body.username || req.body.email;

      const user = await User.findOne({
        where: {
          [Op.or]: [{
            email: email
          }, {
            username: username
          }],
        },
        include: ["userRoles", "sellerid"],
      });

      if (!user) {
        console.log("EMAIL TAK SAMA");
        res.json({
          message: "Can't find that account"
        });
        return;
      }

      const isPasswordCorrect = bcrypt.compareSync(
        pass,
        user.dataValues.encryptedPassword
      );
      console.log(isPasswordCorrect);

      if (!isPasswordCorrect) {
        console.log("PASSWORD TAK SAMA");
        res.json({
          message: "Can't find that account"
        });
        return;
      }

      const token = await authController.createToken({
          id: user.dataValues.id,
          email: user.dataValues.email,
          firstName: user.dataValues.firstName,
          lastName: user.dataValues.lastName,
          fullname: user.dataValues.fullname,
          username: user.dataValues.username,
          address: user.dataValues.address,
          sellerId: user.dataValues.sellerId,
          phoneNumber: user.dataValues.phoneNumber,
          image: user.dataValues.image,
          city: user.dataValues.city,
        },
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );

      const newToken = {
        nama: user.dataValues.fullname,
        ID: user.dataValues.id,
        Identification: user.dataValues.userRoles.name,
        token,
      };

      res.cookie("auth_token", token, {
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000,
        sameSite: "lax",
        secure: false,
      });
      res.json({
        token
      });
      console.log(req.cookies.auth_token);

      // return res.cookie("Bearer ", token, {
      //   httpOnly: true,
      //   secure: process.env.NODE_ENV === "production"
      // }).status(201).json({
      //   newToken,
      // });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: "Your password or email might be wrong, try another one",
      });
    }
  },

  async delete(req, res) {
    try {
      const id = req.params.id;
      const result = await User.destroy({
        where: {
          [Op.and]: [{
            id
          }],
        },
      });
      if (!result) {
        res.json(400).json({
          deletedby: req.result,
          message: "User not found",
        });
      }

      res.json({
        message: "User successfully deleted",
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        message: "something went wrong with the server",
      });
    }
  },

  async deleteSeller(req, res) {
    try {
      const token = req.tokenPayload;
      const userId = token.id;
      const user = await User.findAll({
        where: {
          id: userId
        }
      });
      console.log(user);

      const sellerId = user[0].dataValues.sellerId;
      const seller = await Seller.findByPk(sellerId);

      await User.update({
        sellerId: null,
      }, {
        where: {
          id: userId
        }
      });

      await Seller.destroy({
        where: {
          id: sellerId,
        },
      });

      res.status(201).json({
        message: "Seller has been deleted",
        deleted_seller: seller.dataValues.name,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: "Can't delete the file",
      });
    }
  },

  async getUser(req, res) {
    try {
      try {
        const bearerToken = req.headers.authorization;
        const token = bearerToken.split("Bearer ")[1];
        console.log("TOKEN", token);
        const tokenPayload = jwt.verify(
          token,
          process.env.JWT_SIGNATURE_KEY || "Rahasia"
        );
        const id = tokenPayload.id;
        const requestUser = await User.findByPk(tokenPayload.id, {
          include: ["userRoles", "sellerid"],
        });
        const user = await User.findOne({
          where: {
            id
          },
          include: ["userRoles", "sellerid"],
        });
        if (requestUser.userRoleId) {
          res.status(200).json({
            user: {
              users: user.dataValues,
              position: user.dataValues.userRoles.name,
              seller: user.dataValues.sellerid,
              roleId: user.dataValues.userRoles.id,
            },
          });
        }
      } catch (error) {
        try {
          const bearerToken = req.headers.authorization;
          const token = bearerToken.split("Bearer ")[1];
          const decodeValue = await admin.auth().verifyIdToken(token);
          if (decodeValue) {
            req.tokenPayload = decodeValue;
          }

          res.json({
            message: "Unauthorized Access",
            result: decodeValue
          });
        } catch (error) {
          console.log(error);
          res.json({
            message: "can't get user"
          })
        }
      }
    } catch (err) {
      console.log(err);
      res.json({
        data: null,
        message: "Not enough resources to find the data",
      });
    }
  },

  async getSeller(req, res) {
    try {
      const token = req.tokenPayload;
      const id = token.id;
      const requestUser = await User.findAll({
        where: {
          id
        },
        include: ["sellerid"],
      });
      console.log(requestUser);
      res.status(200).json({
        seller: {
          storeName: requestUser[0].dataValues.sellerid.nama,
        },
      });
    } catch (error) {
      res.status(403).json({
        message: "Authorization error",
      });
    }
  },

  async updateUser(req, res) {
    try {
      const token = req.tokenPayload;
      const id = token.id;
      const initial = await User.findAll({
        where: {
          id
        }
      });

      uploadOnMemory.single("picture")(req, res, async function () {
        if (!req.file) {
          await User.update({
            firstName: req.body.firstname ?
              req.body.firstname : initial.firstName,
            lastName: req.body.lastname ?
              req.body.lastname : initial.lastName,
            fullname: req.body.fullname ?
              req.body.fullname : initial.fullname,
            username: req.body.username ?
              req.body.username : initial.username,
            address: req.body.address ? req.body.address : initial.address,
            phoneNumber: req.body.nomorhp ?
              req.body.nomorhp : initial.phoneNumber,
            image: initial.image,
            city: req.body.city ? req.body.city : initial.city,
          }, {
            where: {
              id
            }
          })

          return res.status(204).json({
            message: "UPDATENYA BERHASIL BRO"
          })
        }
        const fileBase64 = req.file.buffer.toString("base64");
        const file = `data:${req.file.mimetype};base64,${fileBase64}`;
        const url = `/uploads/$`;

        cloudinary.uploader.upload(file, async function (err, result) {
          if (err) {
            console.log(err);
            return res.status(400).json({
              message: "gagal upload file",
            });
          }
          console.log(result.url);
          const user = await User.update({
            firstName: req.body.firstname ?
              req.body.firstname : initial.firstName,
            lastName: req.body.lastname ?
              req.body.lastname : initial.lastName,
            fullname: req.body.fullname ?
              req.body.fullname : initial.fullname,
            username: req.body.username ?
              req.body.username : initial.username,
            address: req.body.address ? req.body.address : initial.address,
            phoneNumber: req.body.nomorhp ?
              req.body.nomorhp : initial.phoneNumber,
            image: result.url ? result.url : initial.image,
            city: req.body.city ? req.body.city : initial.city,
          }, {
            where: {
              id
            }
          });

          return res.status(204).json({
            status: "OK",
            message: "User succesfully updated",
          });

        });
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: "user not found",
      });
    }
  },

  async updateSeller(req, res) {
    try {
      const token = req.tokenPayload;
      const idUser = token.id;
      const user = await User.findAll({
        where: {
          id: idUser
        }
      });
      const sellerId = user[0].dataValues.sellerId;
      const seller = await Seller.findByPk(sellerId);
      console.log("SELLER CURRENT", seller.dataValues.nama);

      await Seller.update({
        nama: req.body.name,
      }, {
        where: {
          id: sellerId
        }
      });

      const updatedSeller = await Seller.findByPk(sellerId);
      console.log("UPDATED SELLER", updatedSeller.dataValues.nama);

      res.status(201).json({
        message: "Seller data has been updated",
        old_name: seller.dataValues.nama,
        updated_name: updatedSeller.dataValues.nama,
      });
    } catch (error) {
      console.log(error);
      res.json({
        message: "Can't update seller",
      });
    }
  },

  async listUser(req, res) {
    try {
      const accessedBy = req.result;
      const initial = await User.findAll();
      res.status(200).json({
        accessedBy: accessedBy.user.name,
        position: accessedBy.user.position,
        message: "User List",
        user: initial,
      });
    } catch (error) {
      console.log(error);
      res.json(400).json({
        message: "unauthorized acccess",
      });
    }
  },

  async logOut(req, res) {
    req.headers.authorization = "";
    localStorage.removeItem("auth_token");
  },

  // async forgotPassword(req, res) {
  //   const { email } = req.body;

  //   await User.findAll({ where: { email } }, (err, user) => {

  //     if(err || !user){
  //       return res.status(400).json({
  //         message: "User with this email does not exist"
  //       });

  //       const token = jwt.sign({fullname, email, password}, process.env.JWT_SIGNATURE_KEY || 'Rahasia'. {expiresIn: '20m'});
  //       const data = {
  //         from: 'noreply@hello.com',
  //         to: email,
  //         subject: 'Account Password Reset Link',
  //         html: `
  //               <h2>Please click on given link to reset your password</h2>
  //               <p>${process.env.CLIENT_URL}/authentication/resetPassword/${token}</p>
  //         `
  //       }
  //     }
  //   });
  // },
};