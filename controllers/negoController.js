const {
  Barang,
  User,
  Seller,
  Category,
  Sequelize,
  Photo,
  Negosiasi,
  Notifikasi,
} = require("../models");
const upload = require("../upload");
const uploadOnMemory = require("../uploadOnMemory");
const cloudinary = require("../cloudinary");
const authController = require("./authController");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const {
  Op,
  where
} = require("sequelize");

module.exports = {
  async createNego(req, res) {
    try {
      const token = req.tokenPayload;
      let idBarang = req.params.id;

      if (!token) {
        res.status(403).json({
          message: "Unauthorized Access",
        });
      }

      const currentItem = await Barang.findAll({
        where: {
          id: idBarang
        }
      });

      console.log("CURRENTITEM", currentItem[0].dataValues.postedBy);

      if (!currentItem) {
        res.status(400).json({
          message: "Item Not Found",
        });
      }

      const nego = await Negosiasi.create({
        itemId: currentItem[0].dataValues.id,
        sendId: token.id,
        receiverId: currentItem[0].dataValues.postedBy,
        tawar: req.body.tawar,
        isAccept: "TBD",
      });

      const notif = await Notifikasi.create({
        barangId: currentItem[0].dataValues.id,
        harga: currentItem[0].dataValues.price,
        tawar: req.body.tawar,
        seen_by_user: 0,
        userId: currentItem[0].dataValues.postedBy,
        senderId: token.id,
        status: "TBD",
        categoryId: 1,
      });

      res.status(201).json({
        message: "Negosiasi Created",
        nego: nego,
        notif: notif,
      });
    } catch (error) {
      console.log(error);
      res.json({
        message: "An error occured",
        error: error,
      });
    }
  },

  async findNego(req, res) {
    const token = req.tokenPayload;
    const nego = await Negosiasi.findAll({
      where: {
        [Op.or]: [{
          sendId: token.id
        }, {
          receiverId: token.id
        }],
      },
      include: ["sendid", "receiverid", "itemid"],
    });

    console.log(nego);

    res.status(200).json({
      message: "this is your nego list",
      nego: nego,
    });
  },

  async findNegoById(req, res) {
    const token = req.tokenPayload;
    const id = req.params.id;

    const nego = await Negosiasi.findAll({
      where: {
        [Op.and]: [{
            id: id
          },
          {
            [Op.or]: [{
              sendId: token.id
            }, {
              receiverId: token.id
            }],
          },
        ],
      },
      include: ["itemid", "receiverid", "sendid"]
    });

    res.status(200).json({
      message: "this is your nego",
      nego: nego,
    });
  },

  async updateNego(req, res) {
    try {
      const token = req.tokenPayload;
      const decision = req.body.decision;
      const terima = req.body.jual;
      const idNego = req.params.id;
      const currentUser = token.id

      const currentNego = await Negosiasi.findAll({
        where: {
          id: idNego
        }
      });

      console.log("CURRENTUSERID", token.id);
      console.log("CURRENTNEGO", currentNego[0].dataValues);

      await Negosiasi.update({
        isAccept: req.body.decision ?
          decision : currentNego[0].dataValues.isAccept,
        tawar: req.body.tawar ? req.body.tawar : currentNego[0].dataValues.tawar
      }, {
        where: {
          id: idNego
        }
      });

      if (decision === "ACCEPT") {
        try {
          const currentNotif = await Notifikasi.findAll({
            where: {
              [Op.and]: [{
                  barangId: currentNego[0].dataValues.itemId
                },
                {
                  senderId: currentNego[0].sendId
                }
              ]
            },
          });

          console.log("CurrentNotif", currentNotif[0].dataValues);
          const idNotif = currentNotif[0].dataValues.id;
          console.log("IDNOTIF", idNotif);

          const result = await Notifikasi.update({
            status: "ACCEPT",
            tawar: currentNego[0].dataValues.tawar
          }, {
            where: {
              id: idNotif
            }
          });

          const hasil = await Notifikasi.findAll({
            where: {
              id: currentNotif[0].dataValues.id
            }
          })

          return res.json({
            message: "Negosiasi and Notification Updated",
            result: hasil
          });
        } catch (error) {
          console.log(error);
          res.json({
            message: error
          })
        }
      } else if (decision === "REJECT") {
        try {
          const currentNotif = await Notifikasi.findAll({
            where: {
              [Op.and]: [{
                  barangId: currentNego[0].dataValues.itemId
                },
                {
                  senderId: currentNego[0].sendId
                }
              ]
            },
          });

          const result = await Notifikasi.update({
            status: "REJECT",
          }, {
            where: {
              id: currentNotif[0].datavalues.id
            }
          });

          const hasil = await Notifikasi.findAll({
            where: {
              id: currentNotif[0].dataValues.id
            }
          })

          return res.json({
            message: "Negosiasi and Notification Updated",
            result: hasil
          });
        } catch (error) {
          console.log(error);
          res.json({
            message: error
          })
        }
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: "Something went wrong",
      });
    }
  },

  async negoTerjual(req, res) {
    try {
      const token = req.tokenPayload;
      const idNego = req.params.id;
      const isOk = await req.body.decision;

      const currentNego = await Negosiasi.findAll({
        where: {
          id: idNego
        }
      });

      const currentBarang = await Barang.findAll({
        where: {
          [Op.and]: [{
              id: currentNego[0].dataValues.itemId
            },
            {
              postedBy: currentNego[0].dataValues.receiverId
            }
          ]
        }
      })

      await Barang.update({
        isTerjual: req.body.decision ? isOk : currentBarang[0].dataValues.isTerjual
      }, {
        where: {
          [Op.and]: [{
              id: currentBarang[0].dataValues.id
            },
            {
              postedBy: currentBarang[0].dataValues.postedBy
            }
          ]
        }
      })
      const currentNotif = await Notifikasi.findAll({
        where: {
          [Op.and]: [{
              barangId: currentNego[0].dataValues.itemId
            },
            {
              senderId: currentNego[0].sendId
            }
          ]
        },
      });
      const idNotif = currentNotif[0].dataValues.id;

      if(currentNotif){
        const result = await Notifikasi.update({
          status: "TERJUAL",
          tawar: currentNego[0].dataValues.tawar
        }, {
          where: {
            id: idNotif
          }
        });
      }

      const hasil = await Notifikasi.findAll({
        where: {
          id: currentNotif[0].dataValues.id
        }
      })

      const negoLatest = await Negosiasi.findAll({
        where: {
          id: idNego
        }
      })

      await Negosiasi.update({
        isTerjual: req.body.decision ?
          isOk : currentNego[0].dataValues.isTerjual,
        tawar: req.body.tawar ? req.body.tawar : currentNego[0].dataValues.tawar,
      }, {
        where: {
          id: idNego
        }
      });

      res.json({
        message: "Negosiasi and Notification Updated",
      });
    } catch (error) {
      console.log(error);
      res.json({
        message: error
      })
    }
  },

  async negoTerjualTolak(req, res) {
    const token = req.tokenPayload;
    const idNego = req.params.id;
    const isOk = await req.body.decision;

    const currentNego = await Negosiasi.findAll({
      where: {
        id: idNego
      }
    });

    const currentBarang = await Barang.findAll({
      where: {
        [Op.and]: [{
            id: currentNego[0].dataValues.itemId
          },
          {
            postedBy: currentNego[0].dataValues.receiverId
          }
        ]
      }
    })

    await Barang.update({
      isTerjual: req.body.decision ? isOk : currentBarang[0].dataValues.isTerjual
    }, {
      where: {
        [Op.and]: [{
            id: currentBarang[0].dataValues.id
          },
          {
            postedBy: currentBarang[0].dataValues.postedBy
          }
        ]
      }
    })

    const currentNotif = await Notifikasi.findAll({
      where: {
        [Op.and]: [{
            barangId: currentNego[0].dataValues.itemId
          },
          {
            senderId: currentNego[0].sendId
          }
        ]
      },
    });

    if(currentNotif){
      await Notifikasi.update({
        status: "REJECT",
      }, {
        where: {
          id: currentNotif[0].dataValues.id
        }
      });
    }

    await Negosiasi.update({
      isTerjual: "BATAL",
      isAccept: "REJECT",
    }, {
      where: {
        id: idNego
      }
    });

    res.json({
      message: "Negosiasi and Notification Updated",
    });

  },

  async updateNegoBarang(req, res) {
    try {
      const token = req.tokenPayload;
      const decision = req.body.decision;
      const idBarang = req.params.id;
      const currentUser = token.id

      const currentNego = await Negosiasi.findAll({
        where: {
          [Op.and]: [{
              itemId: idBarang
            },
            {
              sendId: currentUser
            }
          ]
        }
      });

      console.log("CURRENTUSERID", token.id);
      console.log("CURRENTNEGO", currentNego[0].dataValues);

      await Negosiasi.update({
        isAccept: "",
        isTerjual: "",
        tawar: req.body.tawar ? req.body.tawar : currentNego[0].dataValues.tawar
      }, {
        where: {
          id: currentNego[0].dataValues.id
        }
      });

      const currentNotif = await Notifikasi.findAll({
        where: {
          [Op.and]: [{
              barangId: currentNego[0].dataValues.itemId
            },
            {
              senderId: currentNego[0].sendId
            }
          ]
        },
      });

      console.log("CurrentNotif", currentNotif[0].dataValues);
      const idNotif = currentNotif[0].dataValues.id;
      console.log("IDNOTIF", idNotif);

      const result = await Notifikasi.update({
        status: "TBD",
        tawar: currentNego[0].dataValues.tawar
      }, {
        where: {
          id: idNotif
        }
      });

      res.json({
        message: "Negosiasi and Notification Updated",
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: "Something went wrong",
      });
    }
  },

  async findNegoforUser(req, res) {
    try {
      const token = req.tokenPayload;
      const idBarang = req.params.id;

      const nego = await Negosiasi.findAll({
        where: {
          [Op.and]: [{
              sendId: token.id
            },
            {
              itemId: idBarang
            }
          ]
        },
        include: ["itemid", "sendid", "receiverid"]
      })

      res.status(200).json({
        message: "this is your negosiasi for this particular item",
        data: nego
      })
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: "Can't find nego",
        error: error
      })
    }
  },

  async directBuy(req, res){
    try {
      const token = req.tokenPayload;
      let idBarang= req.params.id;

      if(!token){
        res.status(400).json({
          message: "Unauthorized access"
        })
      }

      const currentItem = await Barang.findAll({
        where: {
          id: idBarang
        }
      })

      const itemOwner = await currentItem[0].dataValues.postedBy;

      if(!currentItem){
        res.status(400).json({
          message: "This item doesn't exist"
        })
      }

      const nego = await Negosiasi.create({
        itemId: idBarang,
        sendId: token.id,
        receiverId: itemOwner,
        tawar: currentItem[0].dataValues.price,
        isAccept: "ACCEPT"
      });

      const notif = await Notifikasi.create({
        barangId: idBarang,
        harga: currentItem[0].dataValues.price,
        seen_by_user: 0,
        userId: itemOwner,
        senderId: token.id,
        status: "ACCEPT",
        categoryId: 1,
      })

      res.status(201).json({
        message: "Negosiasi Created",
        nego: nego,
        notif: notif,
      })
    } catch (error) {
      console.log(error);
      res.json({
        message: "An error occured",
        error: error
      })
    }
  },

  async directBuyUpdate(req, res){
    try {
      const token = req.tokenPayload;
      let idBarang= req.params.id;

      if(!token){
        res.status(400).json({
          message: "Unauthorized access"
        })
      }

      const currentItem = await Barang.findAll({
        where: {
          id: idBarang
        }
      })

      const itemOwner = await currentItem[0].dataValues.postedBy;

      if(!currentItem){
        res.status(400).json({
          message: "This item doesn't exist"
        })
      }

      const currentNego = await Negosiasi.findAll({
        where: {
          [Op.and]: [
            {itemId: idBarang},
            {sendId: token.id}
          ]
        }
      })

      const nego = await Negosiasi.update({
        itemId: idBarang,
        sendId: token.id,
        receiverId: itemOwner,
        tawar: currentItem[0].dataValues.price,
        isAccept: "ACCEPT"
      }, {
        where: {
          id: currentNego[0].dataValues.id
        }
      });

      const currentNotif = await Notifikasi.findAll({
        where: {
          [Op.and]: [
            {barangId: idBarang},
            {senderId: token.id}
          ]
        }
      })

      const notif = await Notifikasi.update({
        barangId: idBarang,
        harga: currentItem[0].dataValues.price,
        seen_by_user: 0,
        userId: itemOwner,
        senderId: token.id,
        status: "ACCEPT",
        categoryId: 1,
      }, {
        where: {
          id: currentNotif[0].dataValues.id
        }
      })

      res.json({
        message: "Negosiasi Created",
        nego: nego,
        notif: notif,
      })
    } catch (error) {
      console.log(error);
      res.json({
        message: "An error occured",
        error: error
      })
    }
  }, 
};