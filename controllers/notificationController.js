const { Op } = require("sequelize");
const { Notifikasi, Barang, User } = require("../models");

module.exports = {
  async createNotifTawar(req, res) {
    try {
      const token = req.tokenPayload;
      let tawar = Number(req.body.nego);
      let hargaTawar = tawar.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

      const barang = await Barang.findAll({ where: { id: req.params.id } });
      const receiver = barang[0].dataValues.postedBy;

      let hargaBarang = barang[0].dataValues.price;
      let hargaNew = hargaBarang;

      await Notifikasi.create({
        barang: barang[0].dataValues.nama_barang,
        harga: hargaNew,
        extended: `Ditawar Rp ${hargaTawar}`,
        seen_by_user: 1,
        userId: receiver,
        senderId: token.id,
        categoryId: 1,
      });

      const ress = await Notifikasi.findAll(
        {
          limit: 1,
          order: [["createdAt", "DESC"]],
        },
        {
          where: { senderId: token.id },
        }
      );

      res.status(201).json({
        ress,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "FAIL"
      })
    }
  },

  async createNotifCreate(req, res) {
    try {
      const token = req.tokenPayload;

      const barang = await Barang.findAll(
        {
          limit: 1,
          order: [["createdAt", "DESC"]],
        },
        {
          where: {
            [Op.and]: [{ postedBy: token.id }, { id: req.params.id }],
          },
        }
      );

      console.log(barang);

      const receiver = token.id;

      await Notifikasi.create({
        barangId: barang[0].dataValues.id,
        barang: barang[0].dataValues.nama_barang,
        harga: barang[0].dataValues.price,
        seen_by_user: 1,
        userId: receiver,
        senderId: token.id,
        categoryId: 2,
      });

      const ress = await Notifikasi.findAll(
        {
          limit: 1,
          order: [["createdAt", "DESC"]],
        },
        {
          where: { senderId: token.id },
        }
      );

      res.status(201).json({
        ress,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: error,
        status: "FAIL"
      })
    }
  },

  async deleteNotifById(req, res) {
    try {
      const token = req.tokenPayload;

      await Notifikasi.destroy({
        where: {
          [Op.and]: [{ id: req.params.id }, { senderId: token.id }],
        },
      });

      res.status(201).json({
        message: "notification has been deleted",
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "FAIL"
      })
    }
  },

  async deleteAllNotif(req, res) {
    try {
      const token = req.tokenPayload;

      await Notifikasi.destroy({
        where: {
          senderId: token.id,
        },
      });

      res.status(201).json({
        message: "notification has been deleted",
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "FAIL"
      })
    }
  },

  async readNotif(req, res){
    try {
      const token = req.tokenPayload;

      const ressTawar = await Notifikasi.findAll(
        {
          where: {
            [Op.or]: [{userId: token.id}, {senderId: token.id}]
          },
          include: ["categoryid", "senderid", "userid", "barangid"],
          order: [["updatedAt", "DESC"]]
        }
      );

      res.status(200).json({
        message: "Berhasil",
        data: ressTawar
      })
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "FAIL"
      })
    }
  },

  // async readNotifTerbit(req, res){
  //   try {
  //     const token = req.tokenPayload;

  //     const ress = await Notifikasi.findAll(
  //       {
  //         where: {
  //           userId: token.id
  //         }
  //       }
  //     )
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }

  async updateNotifById(req, res) {
    try {
      const token = req.tokenPayload;

      const ress = await Notifikasi.update(
        {
          seen_by_user: 0,
        },
        {
          where: {
            [Op.and]: [{ id: req.params.id }, { senderId: token.id }],
          },
        }
      );

      res.status(201).json({
        message: "Notification has been updated",
        ress,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "FAIL"
      })
    }
  },

  async updateNotif(req, res) {
    try {
      const token = req.tokenPayload;

      const ress = await Notifikasi.update(
        {
          seen_by_user: 0,
        },
        {
          where: {
            senderId: token.id,
          },
        }
      );

      res.status(201).json({
        message: "Notification has been updated",
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "FAIL"
      })
    }
  },

  async restoreNotif(req, res){
    try {
      const token = req.tokenPayload;
      const idNotif = req.params.id;

      const findNotif = await Notifikasi.findAll({
        where: {
          [Op.and] : [
            {id: idNotif},
            {senderId: token.did}
          ]
        },
          paranoid: false
      })

      if(findNotif == null || undefined || "" || findNotif.length == 0){
        return res.json({
          message: "You are currently searching for inexisting notif"
        })
      }

      if(findNotif.dataValues.deletedAt == null || undefined){
        return res.json({
          message: "Your notification is still available"
        })
      }

      const restoredNotif = await Notifikasi.restore({
        where: {
          id: idNotif
        }
      })

      res.json({
        message: "Notification is restored successfully",
        notification: restoredNotif
      })
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: "something went wrong",
        error
      })
    }
  }

  // async readNotif(req, res) {
  //   try {
  //     const token = req.tokenPayload;

  //     const ress = await Notifikasi.findAll({
  //       where: {
  //         senderId: token.id,
  //       },
  //     });

  //     res.status(200).json({
  //       ress,
  //     });
  //   } catch (error) {
  //     console.log(error);
  //   }
  // },

  // async readNotifById(req, res) {
  //   try {
  //     const token = req.tokenPayload;

  //     const ress = await Notifikasi.findAll({
  //       where: {
  //         [Op.and]: [{ id: req.params.id }, { senderId: token.id }],
  //       },
  //     });

  //     res.status(200).json({
  //       ress,
  //     });
  //   } catch (error) {
  //     console.log(error);
  //   }
  // },

  // async readNotifUser(req, res) {
  //   try {
  //     const token = req.tokenPayload;

  //     const ress = await Notifikasi.findAll({
  //       where: {
  //         userId: token.id,
  //       },
  //     });

  //     res.status(200).json({
  //       ress,
  //     });
  //   } catch (error) {
  //     console.log(error);
  //   }
  // },

  // async readNotifUserById(req, res) {
  //   try {
  //     const token = req.tokenPayload;

  //     const ress = await Notifikasi.findAll({
  //       where: {
  //         [Op.and]: [{ id: req.params.id }, { userId: token.id }],
  //       },
  //     });

  //     res.status(200).json({
  //       ress,
  //     });
  //   } catch (error) {
  //     console.log(error);
  //   }
  // },
};
