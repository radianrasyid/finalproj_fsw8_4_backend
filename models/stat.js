"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Stat extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasOne(models.Seller, {
        foreignKey: "statsId",
        as: "statsid",
      });
    }
  }
  Stat.init(
    {
      pendapatan: DataTypes.INTEGER,
      sold_prod: DataTypes.INTEGER,
      new_order: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Stat",
    }
  );
  return Stat;
};
