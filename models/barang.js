"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Barang extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.User, {
        foreignKey: "postedBy",
        as: "postedby",
      });
      this.hasMany(models.OrderDetail, {
        foreignKey: "productId",
        as: "productid",
      });
      this.hasMany(models.Negosiasi, {
        foreignKey: "itemId",
        as: "itemid"
      })
      this.hasMany(models.Notifikasi, {
        foreignKey: "barangId",
        as: "barangid"
      });
      this.belongsTo(models.Category, {
        foreignKey: "categoryId",
        as: "categoryid"
      })
    }
  }
  Barang.init(
    {
      nama_barang: DataTypes.STRING,
      price: DataTypes.INTEGER,
      discount: DataTypes.INTEGER,
      berat: DataTypes.INTEGER,
      description: DataTypes.TEXT,
      isAvailable: DataTypes.INTEGER,
      quantity: DataTypes.INTEGER,
      image: DataTypes.ARRAY(DataTypes.STRING),
      isTerjual: DataTypes.INTEGER,
      postedBy: {
        type: DataTypes.INTEGER,
        references: {
          model: "User",
          key: "id",
        },
      },
      categoryId: {
        type: DataTypes.INTEGER,
        references: {
          model: "Category",
          key: "id",
        },
      },
    },
    {
      sequelize,
      paranoid: true,
      modelName: "Barang",
    }
  );
  return Barang;
};
