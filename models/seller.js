"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Seller extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Stat, {
        foreignKey: "statsId",
        as: "statsid",
      });
      this.belongsTo(models.User, {
        foreignKey: "userId",
        as: "userid",
      });
      this.hasOne(models.User, {
        foreignKey: "sellerId",
        as: "sellerid",
        onDelete: "CASCADE",
      });
    }
  }
  Seller.init(
    {
      nama: DataTypes.STRING,
      statsId: {
        type: DataTypes.INTEGER,
        references: {
          model: "Stat",
          key: "id",
        },
      },
      Status: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Seller",
    }
  );
  return Seller;
};
