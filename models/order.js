"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.User, {
        foreignKey: "buyerId",
        as: "buyerid",
      });
      this.belongsTo(models.Ekspedisi, {
        foreignKey: "shipmentId",
        as: "shipmentid",
      });
      this.belongsTo(models.Barang, {
        foreignKey: "productId",
        as: "productid",
      });
    }
  }
  Order.init(
    {
      buyerId: DataTypes.INTEGER,
      shipmentId: DataTypes.INTEGER,
      orderDate: DataTypes.DATE,
      shippedDate: DataTypes.DATE,
      orderNumber: DataTypes.INTEGER,
      productId: DataTypes.INTEGER,
      weight: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Order",
    }
  );
  return Order;
};
