"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.userRole, {
        foreignKey: "userRoleId",
        as: "userRoles",
      });

      this.hasMany(models.Barang, {
        foreignKey: "postedBy",
        as: "postedby",
      });

      this.hasMany(models.Order, {
        foreignKey: "buyerId",
        as: "buyerid",
      });

      this.belongsTo(models.Seller, {
        foreignKey: "sellerId",
        as: "sellerid",
        onDelete: "CASCADE",
      });

      this.hasMany(models.Notifikasi, {
        foreignKey: "userId",
        as: "userid",
      });

      this.hasMany(models.Notifikasi, {
        foreignKey: "senderId",
        as: "senderid",
      });

      this.hasMany(models.Negosiasi, {
        foreignKey: "sendId",
        as: "sendid"
      });

      this.hasMany(models.Negosiasi, {
        foreignKey: "receiverId",
        as: "receiverid"
      });
    }
  }
  User.init(
    {
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      fullname: DataTypes.STRING,
      username: {
        type: DataTypes.STRING,
        unique: true
      },
      image: DataTypes.STRING,
      encryptedPassword: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        unique: true,
        validate: {
          isEmail: true
        }
      },
      address: DataTypes.STRING,
      phoneNumber: DataTypes.STRING,
      city: DataTypes.STRING,
      userRoleId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "userRole",
          key: "id",
        },
      },
      sellerId: {
        type: DataTypes.INTEGER,
        references: {
          model: "Seller",
          key: "id",
        },
      },
      resetLink: {
        type: DataTypes.STRING
      }
    },
    {
      sequelize,
      modelName: "User",
    }
  );
  return User;
};
