"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Notifikasi extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.User, {
        foreignKey: "userId",
        as: "userid",
      });

      this.belongsTo(models.User, {
        foreignKey: "senderId",
        as: "senderid",
      });
      this.belongsTo(models.notificationCategory, {
        foreignKey: "categoryId",
        as: "categoryid",
      });
      this.belongsTo(models.Barang, {
        foreignKey: "barangId",
        as: "barangid"
      })
    }
  }
  Notifikasi.init(
    {
      barangId: {
        type: DataTypes.INTEGER,
        references: {
          model: "Barang",
          key: "id"
        }
      },
      harga: DataTypes.INTEGER,
      tawar: DataTypes.INTEGER,
      status: DataTypes.STRING,
      seen_by_user: DataTypes.INTEGER,
      userId: {
        type: DataTypes.INTEGER,
        references: {
          model: "User",
          key: "id",
        },
      },
      senderId: {
        type: DataTypes.INTEGER,
        references: {
          model: "User",
          key: "id",
        },
      },
      categoryId: {
        type: DataTypes.INTEGER,
        references: {
          model: "notificationCategory",
          key: "id",
        },
      },
    },
    {
      sequelize,
      modelName: "Notifikasi",
      paranoid: true
    }
  );
  return Notifikasi;
};
