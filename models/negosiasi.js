'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Negosiasi extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Barang, {
        foreignKey: "itemId",
        as: "itemid"
      });

      this.belongsTo(models.User, {
        foreignKey: "sendId",
        as: "sendid"
      });

      this.belongsTo(models.User, {
        foreignKey: "receiverId",
        as: "receiverid"
      });
    }
  }
  Negosiasi.init({
    itemId: {
      type: DataTypes.INTEGER,
      references: {
        model: "Barang",
        key: "id"
      }
    },
    sendId: {
      type: DataTypes.INTEGER,
      references: {
        model: "User",
        key: "id"
      }
    },
    receiverId: {
      type: DataTypes.INTEGER,
      references: {
        model: "User",
        key: "id"
      }
    },
    tawar: DataTypes.INTEGER,
    isAccept: DataTypes.STRING,
    isTerjual: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Negosiasi',
  });
  return Negosiasi;
};