"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class notificationCategory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Notifikasi, {
        foreignKey: "categoryId",
        as: "categoryid",
      });
    }
  }
  notificationCategory.init(
    {
      type: DataTypes.STRING,
      text: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "notificationCategory",
    }
  );
  return notificationCategory;
};
