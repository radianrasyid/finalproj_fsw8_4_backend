class Word{
    constructor(kata, array){
        this.kata = kata,
        this.array = [array]
        this.stash = []
    }

    display(kata){
        kata = this.kata
        console.log(this.array);
    }

    tampil(value){
        for(let i = 0; i<this.stash.length; i++){
            if(this.stash[i].kata == value){
                return console.log(this.stash[i]);
            }
        }
    }

    add(kata, array){
        const add = {
            kata,
            array
        }

        this.stash.push(add)
    }
}

const kamus = new Word()

kamus.add('big', ['large', 'great'])
kamus.add('big', ['huge', 'fat'])
kamus.add('huge', ['enormous', 'gigantic'])
kamus.tampil('huge');

